Cot. N° {{ $quote->code }}<br /><br />

{{ $quote->client->name }},<br /><br />

Estimado cliente, según lo<br />
requerido hemos emitido la<br />
cotización N° {{ $quote->code }}.<br />
Para su revisión y pago<br/>
acceda al siguiente enlace<br /><br />

<a href="{{ URL::to('quotes/quote', $quote->id) }}">Aceptar y pagar</a>