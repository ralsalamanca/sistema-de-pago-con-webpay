@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget widget-table action-table">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>@lang('transactions.transactions')</h3>
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>@lang('transactions.id')</th>
							<th>@lang('transactions.quote')</th>
							<th>@lang('transactions.amount')</th>
							<th>@lang('transactions.gateway')</th>
							<th>@lang('transactions.paid_at')</th>
							<th>@lang('transactions.reversed_at')</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
						<tr>
							<td>{{ $transaction->id }}</td>
							<td><a href="{{ URL::route('admin..quotes.show', $transaction->quote_id) }}">{{ $transaction->quote->name }}</a></td>
							<td>{{ $transactions->amount }}</td>
							<td>{{ $transactions->gateway }}</td>
							<td>{{ $transactions->created_at }}</td>
							<td>{{ $transactions->reversed_at }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
@stop