@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header"> <i class="icon-th-list"></i>
				<h3>@lang('clients.clients')</h3>
			</div>
			<div class="widget-content">
				{{ Form::open(array('url' => 'admin/clients', 'class' => 'form-horizontal')) }}
					<fieldset>
						<div class="control-group">
							{{ Form::label('name', trans('clients.name'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('name', Input::old('name'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('surname', trans('clients.surname'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('surname', Input::old('surname'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('rut', trans('clients.rut'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('rut', Input::old('rut'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('email', trans('clients.email'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('email', Input::old('email'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('password', trans('clients.password'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('password', Input::old('password'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">@lang('form.button.save')</button> 
							<button class="btn">@lang('form.button.cancel')</button>
						</div>
					</fieldset>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop