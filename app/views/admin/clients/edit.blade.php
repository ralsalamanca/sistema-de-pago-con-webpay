@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header"> <i class="icon-th-list"></i>
				<h3>{{ $client->fullname }}</h3>
				<a href="{{ url("admin/clients", $client->id) }}" class="btn btn-mini btn-default">@lang('form.button.show')</a>
			</div>
			<div class="widget-content">
				{{ Form::model($client, array('route' => array('admin..clients.update', $client->id), 'method' => 'PUT')) }}
					<fieldset>
						<div class="control-group">
							{{ Form::label('name', trans('clients.name'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('name', null, array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('surname', trans('clients.surname'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('surname', null, array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('rut', trans('clients.rut'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('rut', null, array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('email', trans('clients.email'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('email', null, array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('password', trans('clients.password'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('password', null, array('class' => 'span6')) }}
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">@lang('form.button.save')</button> 
							<button class="btn">@lang('form.button.cancel')</button>
						</div>
					</fieldset>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop