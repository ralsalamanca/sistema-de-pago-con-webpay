@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget widget-table action-table">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>@lang('clients.clients')</h3>
				<!--<a href="{{ url('admin/clients/create') }}" class="btn btn-mini btn-default">Agregar</a>-->
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>@lang('clients.id')</th>
							<th>@lang('clients.fullname')</th>
							<th>@lang('clients.email')</th>
							<th>@lang('clients.rut')</th>
							<th>@lang('clients.created_at')</th>
							<!--<th></th>-->
						</tr>
					</thead>
					<tbody>
						@foreach($clients as $client)
						<tr>
							<td><a href="{{ URL::route('admin..clients.show', $client->id) }}">{{ $client->id }}</a></td>
							<td><a href="{{ URL::route('admin..clients.show', $client->id) }}">{{ $client->fullname }}</a></td>
							<td>{{ $client->email }}</td>
							<td>{{ $client->rut }}</td>
							<td>{{ $client->created_at }}</td>
							<!--<td>
								<a href="{{ url("admin/clients/{$client->id}/edit") }}" class="btn btn-mini btn-success pull-left">@lang('form.button.edit') <i class="btn-icon-only icon-ok"> </i></a>
							    {{ Form::open(array('url' => array('admin/clients', $client->id), 'method' => 'delete', 'class' => 'pull-left', 'style' => 'margin: 0 0 0 5px')) }}
								<buttom type="submit" class="btn btn-danger btn-mini">@lang('form.button.remove') <i class="btn-icon-only icon-remove"> </i></button>
								{{ Form::close() }}
							</td>-->
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
@stop