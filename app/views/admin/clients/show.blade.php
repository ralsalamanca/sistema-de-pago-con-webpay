@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span5">
		<div class="widget widget-table action-table">
			<div class="widget-header"> <i class="icon-th-list"></i>
				<h3>{{ $client->fullname }}</h3>
				<!--<a href="{{ url("admin/clients/{$client->id}/edit") }}" class="btn btn-mini btn-default">@lang('form.button.edit')</a>-->
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<tr>
						<td>@lang('clients.name')</td>
						<td>{{ $client->name }}</td>
					</tr>
					<tr>
						<td>@lang('clients.surname')</td>
						<td>{{ $client->surname }}</td>
					</tr>
					<tr>
						<td>@lang('clients.rut')</td>
						<td>{{ $client->rut }}</td>
					</tr>
					<tr>
						<td>@lang('clients.email')</td>
						<td>{{ $client->email }}</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="widget widget-table action-table">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>@lang('transactions.last')</h3>
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<thead>
						<th>@lang('transactions.id')</th>
						<th>@lang('transactions.quote')</th>
						<th>@lang('transactions.date')</th>
						<th>@lang('transactions.gateway')</th>
						<th>@lang('transactions.amount')</th>
					</thead>
					<tbody>
						@foreach($client->transactions->take(10) as $transaction)
						<tr>
							<td>{{ $transaction->id }}</td>
							<td>{{ $transaction->quote->name }}</td>
							<td>{{ $transaction->paid_at }}</td>
							<td>{{ $transaction->gateway }}</td>
							<td>{{ $transaction->amount }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="span7">
		<div class="widget widget-nopad">
			<div class="widget-header"> <i class="icon-list-alt"></i>
				<h3>@lang('quotes.quotes')</h3>
				<span class="label label-info">@lang('quotes.sent')</span>
				<span class="label label-success">@lang('quotes.paid')</span>
			</div>
            <div class="widget-content">
				<div id="calendar"></div>
            </div>
		</div>
	</div>
</div>
@stop
@section('footer')
<script type="text/javascript">
	$(document).ready(function(){
		events = new Array;

		calendar = $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'	
			},
			selectable: true,
			eventSources: [events]
		});
		@foreach($client->quotes as $quote)
		events.push({
			title: '({{ $quote->id }}) {{ $quote->name }}',
			start: '{{ $quote->sent_at }}',
			allDay: false
        });
		@if($quote->paid_at)
		events.push({
			title: '({{ $quote->id }}) {{ $quote->name }}',
			start: '{{ $quote->paid_at }}',
			color: '#00bb00',
			allDay: false
        });
        @endif
        @endforeach

		calendar.fullCalendar('refetchEvents');
	});
</script>
@stop