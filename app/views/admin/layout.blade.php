<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>{{ Config::get('page.title') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
{{ HTML::style('/css/bootstrap.css') }}
{{ HTML::style('/css/bootstrap-responsive.css') }}
{{ HTML::style('/css/bootstrap-datepicker.css') }}
{{ HTML::style('/css/font-awesome.css') }}
{{ HTML::style('/css/style.css') }}
{{ HTML::style('/css/pages/dashboard.css') }}
{{ HTML::style('/css/selectize.css') }}
{{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600') }}
<!--[if lt IE 9]>
{{ HTML::script('http://html5shim.googlecode.com/svn/trunk/html5.js') }}
<![endif]-->
@yield('header')
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="#">{{ Config::get('page.name') }}</a>
      <button class="btn btn-default btn-small pull-right"><a href="{{ url('logout') }}">@lang('form.button.logout')</a></button>
    </div>
  </div>
</div>
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        @if(Request::is('admin') or Request::is('admin/import*'))<li class="active">@else<li>@endif<a href="{{ url('admin') }}"><i class="icon-dashboard"></i><span>@lang('dashboard.dashboard')</span></a></li>
        @if(Request::is('admin/clients*'))<li class="active">@else<li>@endif<a href="{{ url('admin/clients') }}"><i class="icon-list-alt"></i><span>@lang('clients.clients')</span></a></li>
        @if(Request::is('admin/quotes*'))<li class="active">@else<li>@endif<a href="{{ url('admin/quotes') }}"><i class="icon-facetime-video"></i><span>@lang('quotes.quotes')</span></a></li>
        @if(Request::is('admin/transactions*'))<li class="active">@else<li>@endif<a href="{{ url('admin/transactions') }}"><i class="icon-bar-chart"></i><span>@lang('transactions.transactions')</span></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="main">
  <div class="main-inner">
    <div class="container">
    @section('message')
      @if(Session::has('success_message'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('success_message') }}
      </div>
      @endif
      @if(Session::has('error_message'))
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('error_message') }}
      </div>
      @endif

      @if($errors->count())
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @lang('messages.form.error')
        {{ HTML::ul($errors->all()) }}
      </div>
      @endif
    @show
		@yield('content')
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12">{{ Config::get('page.footer') }}</div>
      </div>
    </div>
  </div>
</div>
{{ HTML::script('/js/jquery.js') }}
{{ HTML::script('/js/excanvas.min.js') }}
{{ HTML::script('/js/chart.min.js') }}
{{ HTML::script('/js/bootstrap.js') }}
{{ HTML::script('/js/bootstrap-datepicker.js') }}
{{ HTML::script('/js/full-calendar/fullcalendar.min.js') }}
{{ HTML::script('/js/selectize.js') }}
{{ HTML::script('/js/base.js') }}
@yield('footer')
</body>
</html>
