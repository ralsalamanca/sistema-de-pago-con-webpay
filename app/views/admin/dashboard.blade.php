@extends('admin.layout')
@section('content')
<div class="row">
  <div class="span6">
    <div class="widget widget-nopad">
      <div class="widget-header"> <i class="icon-list-alt"></i>
        <h3>@lang('import.import')</h3>
      </div>
      <div class="widget-content">
        <div class="widget big-stats-container">
          <div class="widget-content">
            <h6 class="bigstats">@lang('import.message')</h6>
            <div id="big_stats" class="cf text-center">
              <form method="post" action="{{ URL::route('admin..import.quote') }}">
                <input style="width:300px" name="quote_id" /><br />
                <button style="width:310px" class="btn btn-default">@lang('import.validate')</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <i class="icon-th-list"></i>
        <h3>@lang('transactions.transactions')</h3>
      </div>
      <div class="widget-content">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>@lang('transactions.id')</th>
              <th>@lang('transactions.quote')</th>
              <th>@lang('transactions.amount')</th>
              <th>@lang('transactions.gateway')</th>
              <th>@lang('transactions.paid_at')</th>
              <th>@lang('transactions.reversed_at')</th>
            </tr>
          </thead>
          <tbody>
            @foreach($transactions as $transaction)
            <tr>
              <td>{{ $transaction->id }}</td>
              <td><a href="{{ URL::route('admin..quotes.show', $transaction->quote_id) }}">{{ $transaction->quote->name }}</a></td>
              <td>{{ $transactions->amount }}</td>
              <td>{{ $transactions->gateway }}</td>
              <td>{{ $transactions->created_at }}</td>
              <td>{{ $transactions->reversed_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="span6">
    <div class="widget">
      <div class="widget-content" style="padding:15px">
        <div class="form-group text-center">
          <form method="post" action="" style="margin:0">
            <div class="form-inline">
              <input type="text" id="from" data-date-format="yyyy-m-d" name="from" placeholder="@lang('form.from')" class="form-control" />
              <input type="text" id="to" data-date-format="yyyy-m-d" name="to" placeholder="@lang('form.to')" class="form-control"  />
              <button class="btn btn-default">@lang('quotes.filter')</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <i class="icon-th-list"></i>
        <h3>@lang('quotes.latest_unpaid')</h3>
      </div>
      <div class="widget-content">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>@lang('quotes.id')</th>
              <th>@lang('quotes.name')</th>
              <th>@lang('quotes.client')</th>
              <th>@lang('quotes.created_at')</th>
            </tr>
          </thead>
          <tbody>
            @foreach($unpaid_quotes as $quote)
            <tr>
              <td><a href="{{ URL::route('admin..quotes.show', $quote->id) }}">{{ $quote->id }}</a></td>
              <td><a href="{{ URL::route('admin..quotes.show', $quote->id) }}">{{ $quote->name }}</a></td>
              <td>{{ $quote->client->fullname }}</td>
              <td>{{ $quote->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <i class="icon-th-list"></i>
        <h3>@lang('quotes.latest_paid')</h3>
      </div>
      <div class="widget-content">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>@lang('quotes.id')</th>
              <th>@lang('quotes.name')</th>
              <th>@lang('quotes.client')</th>
              <th>@lang('quotes.created_at')</th>
            </tr>
          </thead>
          <tbody>
            @foreach($paid_quotes as $quote)
            <tr>
              <td><a href="{{ URL::route('admin..quotes.show', $quote->id) }}">{{ $quote->id }}</a></td>
              <td><a href="{{ URL::route('admin..quotes.show', $quote->id) }}">{{ $quote->name }}</a></td>
              <td>{{ $quote->client->fullname }}</td>
              <td>{{ $quote->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="widget">
      <div class="widget-content" style="padding:15px">
        <div class="form-group text-center">
          <div class="form-inline">
            <button class="btn btn-default btn-small"><a href="{{ url('admin/export') }}">@lang('form.button.export')</a></button>
            <button class="btn btn-default btn-small"><a href="">@lang('form.button.refresh')</a></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('footer')
<script type="text/javascript">
$(document).ready(function(){
  $('#from, #to').datepicker();
});
</script>
@stop