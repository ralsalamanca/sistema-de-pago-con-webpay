@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>{{ $quote->client->name }}</h3>
			</div>
			<div class="widget-content">
				{{ Form::open([
					'route' => ['admin..quotes.mail', $quote->id],
					'class' => 'form-horizontal'
				]) }}
				<h3 class="pull-right">@lang('quotes.mail.title', ['code' => $quote->code])</h3>
				<div class="clearfix"></div>
				<hr />
				<fieldset>
					<table class="table table-bordered table-condensed">
						<thead>							<tr>
								<th>@lang('quotes.code')</th>
								<th>@lang('quotes.description')</th>
								<th>@lang('quotes.quantity')</th>
								<th>@lang('quotes.unit_price')</th>
								<th>@lang('quotes.price')</th>
							</tr>
						</thead>
						<tbody>
							@foreach($quote->items as $item)
							<tr>
								<td>{{ $item->code }}</td>
								<td>{{ $item->description }}</td>
								<td>{{ $item->quantity }}</td>
								<td>{{ $item->unit_price }}</td>
								<td>{{ $item->price }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" align="right">@lang('quotes.total')</td>
								<td>{{ $quote->price }}</td>
							</tr>
							<tr>
								<td colspan="4" align="right">@lang('quotes.iva_total')</td>
								<td>{{ $quote->iva_price }}</td>
							</tr>
						</tfoot>
					</table>
					<div class="form-actions" align="right">
						@if(empty($quote->client->email))
						<input name="email" id="email" placeholder="@lang('clients.email')" />
						@else
						<input name="email" id="email" type="hidden" placeholder="@lang('clients.email')" value="{{ $quote->client->email }}" />
						@endif
						<button class="btn">@lang('form.button.cancel')</button>
						<button type="submit" class="btn btn-primary">@lang('form.button.send')</button>
					</div>
				</fieldset>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop