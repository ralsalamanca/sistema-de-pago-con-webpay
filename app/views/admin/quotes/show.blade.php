@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span5">
		<div class="widget widget-table action-table">
			<div class="widget-header"> <i class="icon-th-list"></i>
					<h3>
					@if($quote->is_paid)
					<span class="label label-success">@lang('quotes.paid')</span>
					@else
					<span class="label label-danger">@lang('quotes.nonpaid')</span>
					@endif
					{{ $quote->name }}
				</h3>
				<!-- <a href="{{ url("admin/quotes/{$quote->id}/edit") }}" class="btn btn-mini btn-default">@lang('form.button.edit')</a> -->
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<tr>
						<td width="35%">@lang('quotes.name')</td>
						<td>{{ $quote->name }}</td>
					</tr>
					<tr>
						<td>@lang('quotes.description')</td>
						<td>{{ $quote->description }}</td>
					</tr>
					@if($quote->sent_to)
					<tr>
						<td>@lang('quotes.sent_to')</td>
						<td>{{ $quote->sent_to }}</td>
					</tr>
					@endif
					@if($quote->sent_at)
					<tr>
						<td>@lang('quotes.sent_at')</td>
						<td>{{ $quote->sent_at }}</td>
					</tr>
					@endif
					@if($quote->paid_at)
					<tr>
						<td>@lang('quotes.paid_at')</td>
						<td>{{ $quote->paid_at }}</td>
					</tr>
					@endif
					<tr>
						<td>@lang('quotes.created_at')</td>
						<td>{{ $quote->created_at }}</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="widget widget-table action-table">
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<tr>
						<td width="35%">@lang('clients.fullname')</td>
						<td>{{ $quote->client->fullname }}</td>
					</tr>
					<tr>
						<td>@lang('clients.rut')</td>
						<td>{{ $quote->client->rut }}</td>
					</tr>
					<tr>
						<td>@lang('clients.email')</td>
						<td>{{ $quote->client->email }}</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="widget widget-table action-table">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>@lang('transactions.last')</h3>
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<thead>
						<th>@lang('transactions.id')</th>
						<th>@lang('transactions.quote')</th>
						<th>@lang('transactions.date')</th>
						<th>@lang('transactions.gateway')</th>
						<th>@lang('transactions.amount')</th>
					</thead>
					<tbody>
						@foreach($quote->transactions->take(10) as $transaction)
						<tr>
							<td>{{ $transaction->id }}</td>
							<td>{{ $transaction->quote->name }}</td>
							<td>{{ $transaction->paid_at }}</td>
							<td>{{ $transaction->gateway }}</td>
							<td>{{ $transaction->amount }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="span7">
		<div class="widget widget-table action-table">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>@lang('quote_items.quote_items')</h3>
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<thead>
						<th>@lang('quote_items.name')</th>
						<th>@lang('quote_items.description')</th>
						<th>@lang('quote_items.unit_price')</th>
						<th>@lang('quote_items.quantity')</th>
						<th>@lang('quote_items.amount')</th>
					</thead>
					<tbody>
						@foreach($quote->items as $item)
						<tr>
							<td>{{ $item->name }}</td>
							<td>{{ $item->description }}</td>
							<td>{{ $item->unit_price }}</td>
							<td>{{ $item->quantity }}</td>
							<td>{{ $item->price }}</td>
						</tr>
						@endforeach
					</tbody>
					<tfooter>
						<tr>
							<td colspan="4">@lang('quotes.total')</td>
							<td>{{ $quote->price }}</td>
						</tr>
					</tfooter>
				</table>
			</div>
		</div>
	</div>
</div>
@stop