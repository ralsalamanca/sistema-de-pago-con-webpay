@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header"> <i class="icon-th-list"></i>
				<h3>@lang('quotes.quotes')</h3>
			</div>
			<div class="widget-content">
				{{ Form::model($quote, array('route' => array('admin..quotes.update', $quote->id), 'method' => 'PUT', 'class' => 'form-horizontal' )) }}
					<fieldset>
						<div class="control-group">
							{{ Form::label('name', trans('quotes.client'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::select('client_id', $clients->lists('fullname', 'id'), null, array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('name', trans('quotes.name'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('name', Input::old('name'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('surname', trans('quotes.description'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::textarea('description', Input::old('description'), array('class' => 'span6')) }}
							</div>
						</div>
						<div class="control-group">
							<div class="widget">
								<div class="widget-header">
									<h3>@lang('quotes.items')</h3>
									<a href="javascript:addQuoteItem()" class="btn btn-mini btn-default">@lang('form.button.add')</a>
								</div>
								<div class="widget-content">
									<div id="quote-items" class="accordion">
										@if($quote->items)
											@foreach($quote->items as $i => $quote_item)
											{{ Form::hidden("quote_item[{$i}][id]", $quote_item->id) }}
											<div class="accordion-group">
												<div class="accordion-heading">
													<span class="accordion-toggle">
														<a class="btn btn-mini btn-default" onclick="javascript:removeThisQuoteItem(this)">@lang('form.button.remove')</a>
														<a href="#collapse{{ $i }}" data-toggle="collapse" data-parent="#quote-items">@if(!empty($quote_item['name'])) {{ $quote_item['name'] }} @else Item #{{ $i }} @endif</a>
													</span>
												</div>
											    <div id="collapse{{ $i }}" class="accordion-body collapse @if($i==0) in @endif">
													<div class="accordion-inner">
														<div class="control-group">
															{{ Form::label("quote_item[{$i}][name]", trans('quote_items.name'), array('class' => 'control-label')) }}
															<div class="controls">
																{{ Form::text("quote_item[{$i}][name]", $quote_item['name'], array('class' => 'span6')) }}
															</div>
														</div>
														<div class="control-group">
															{{ Form::label("quote_item[{$i}][description]", trans('quote_items.description'), array('class' => 'control-label')) }}
															<div class="controls">
																{{ Form::text("quote_item[{$i}][description]", $quote_item['description'], array('class' => 'span6')) }}
															</div>
														</div>
														<div class="control-group">
															{{ Form::label("quote_item[{$i}][amount]", trans('quote_items.amount'), array('class' => 'control-label')) }}
															<div class="controls">
																{{ Form::text("quote_item[{$i}][amount]", (int)$quote_item['amount'], array('class' => 'span6')) }}
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach
										@else
										<div class="accordion-group">
											<div class="accordion-heading">
												<span class="accordion-toggle">	
													<a class="btn btn-mini btn-default" onclick="javascript:removeThisQuoteItem(this)">@lang('form.button.remove')</a>
													<a href="#collapse1" data-toggle="collapse" data-parent="#quote-items">Item #1</a>
												</span>
											</div>
										    <div id="collapse1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<div class="control-group">
														{{ Form::label('quote_item[1][name]', trans('quote_items.name'), array('class' => 'control-label')) }}
														<div class="controls">
															{{ Form::text('quote_item[1][name]', null, array('class' => 'span6')) }}
														</div>
													</div>
													<div class="control-group">
														{{ Form::label('quote_item[1][description]', trans('quote_items.description'), array('class' => 'control-label')) }}
														<div class="controls">
															{{ Form::text('quote_item[1][description]', null, array('class' => 'span6')) }}
														</div>
													</div>
													<div class="control-group">
														{{ Form::label('quote_item[1][amount]', trans('quote_items.amount'), array('class' => 'control-label')) }}
														<div class="controls">
															{{ Form::text('quote_item[1][amount]', null, array('class' => 'span6')) }}
														</div>
													</div>
												</div>
											</div>
										</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('total', trans('quotes.total'), array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::hidden('_total', Input::old('_total'), array('id' => '_total')) }}
								{{ Form::text('total', Input::old('_total'), array('class' => 'span6', 'id' => 'total', 'disabled' => true)) }}
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">@lang('form.button.save')</button>
							<button class="btn">@lang('form.button.cancel')</button>
						</div>
					</fieldset>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@stop
@section('footer')
<script type="text/javascript">
	var quoteItemNumber = 2;
	$(document).ready(function(){
		$('select[name="client_id"]').selectize();
		$('input[name^="quote_item"]').each(function(i,element){
			if($(element).attr('name').indexOf('amount') >= 0){
				$(element).keyup(totalCalculation);
			}
			else if($(element).attr('name').indexOf('name') >= 0){
				$(element).keyup(changeThisTitle)
			}
		});
		totalCalculation();
	});
	function totalCalculation(){
		total = 0;
		$('input[name^="quote_item"]').each(function(i,element){
			if($(element).attr('name').indexOf('amount') >= 0){
				amount = $(element).val();
				if(amount) total += parseInt(amount);
			}
		});
		$('#total, #_total').val(total);
	}
	function changeThisTitle(){	
		$('a', $(this).closest('.accordion-group').children()).next().html($(this).val());
	}
	function removeThisQuoteItem(element){
		$(element).closest('.accordion-group').remove();
	}
	function addQuoteItem(){
		$('#quote-items')
			.append(
			$('<div>')
				.addClass('accordion-group')
				.append(
				$('<div>')
					.addClass('accordion-heading')
					.append(
					$('<span>')
						.addClass('accordion-toggle')
						.append(
						$('<a>')
							.addClass('btn btn-mini btn-default')
							.html('@lang('form.button.remove')')
							.attr('onclick', 'javascript:removeThisQuoteItem(this)')
						)
						.append(' ')
						.append(
						$('<a>')
							.attr('id', '#collapseTitle' + quoteItemNumber)
							.attr('data-toggle', 'collapse')
							.attr('data-parent', '#quote-items')
							.attr('href', '#collapse' + quoteItemNumber)
							.html('Item #' + quoteItemNumber)
						)
					)
				)
				.append(
				$('<div>')
					.attr('id', 'collapse' + quoteItemNumber)
					.addClass('accordion-body collapse')
					.append(
					$('<div>')
						.addClass('accordion-inner')
						.append(
						$('<div>').addClass('control-group')
							.append(
							$('<label>')
								.addClass('control-label')
								.attr('for', 'quote_item['+quoteItemNumber+'][name]')
								.html('@lang('quote_items.name')')
							)
							.append(
							$('<div>')
								.addClass('controls')
								.append(
								$('<input>')
									.addClass('span6')
									.attr('name', 'quote_item['+quoteItemNumber+'][name]')
									.keyup(changeThisTitle)
								)
							)
						)
						.append(
						$('<div>').addClass('control-group')
							.append(
							$('<label>')
								.addClass('control-label')
								.attr('for', 'quote_item['+quoteItemNumber+'][description]')
								.html('@lang('quote_items.description')')
							)
							.append(
							$('<div>')
								.addClass('controls')
								.append(
								$('<input>')
									.addClass('span6')
									.attr('name', 'quote_item['+quoteItemNumber+'][description]')
								)
							)
						)
						.append(
						$('<div>').addClass('control-group')
							.append(
							$('<label>')
								.addClass('control-label')
								.attr('for', 'quote_item['+quoteItemNumber+'][amount]')
								.html('@lang('quote_items.amount')')
							)
							.append(
							$('<div>')
								.addClass('controls')
								.append(
								$('<input>')
									.addClass('span6')
									.attr('name', 'quote_item['+quoteItemNumber+'][amount]')
									.keyup(totalCalculation)
								)
							)
						)
					)
				)
			);
		quoteItemNumber++;
	}
</script>
@stop