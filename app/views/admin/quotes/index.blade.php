@extends('admin.layout')
@section('content')
<div class="row">
	<div class="span12">
		<div class="widget widget-table action-table">
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3>@lang('quotes.quotes')</h3>
				<!-- <a href="{{ url('admin/quotes/create') }}" class="btn btn-mini btn-default">Agregar</a>-->
			</div>
			<div class="widget-content">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>@lang('quotes.id')</th>
							<th>@lang('quotes.name')</th>
							<th>@lang('quotes.client')</th>
							<th>@lang('quotes.created_at')</th>
							<!--<th></th>-->
						</tr>
					</thead>
					<tbody>
						@foreach($quotes as $quote)
						<tr>
							<td><a href="{{ URL::route('admin..quotes.show', $quote->id) }}">{{ $quote->id }}</a></td>
							<td><a href="{{ URL::route('admin..quotes.show', $quote->id) }}">{{ $quote->name }}</a></td>
							<td>{{ $quote->client->fullname }}</td>
							<td>{{ $quote->created_at }}</td>
							<!--<td>
								<a href="{{ url("admin/quotes/{$quote->id}/edit") }}" class="btn btn-mini btn-success pull-left">@lang('form.button.edit') <i class="btn-icon-only icon-ok"> </i></a>
							    {{ Form::open(array('url' => array('admin/quotes', $quote->id), 'method' => 'delete', 'class' => 'pull-left', 'style' => 'margin: 0 0 0 5px')) }}
								<buttom type="submit" class="btn btn-danger btn-mini">@lang('form.button.remove') <i class="btn-icon-only icon-remove"> </i></button>
								{{ Form::close() }}
							</td>-->
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop