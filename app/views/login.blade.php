
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ Config::get('page.title') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
	{{ HTML::style('/css/bootstrap.css') }}
	{{ HTML::style('/css/bootstrap-responsive.css') }}
	{{ HTML::style('/css/font-awesome.css') }}
	{{ HTML::style('/css/style.css') }}
	{{ HTML::style('/css/pages/dashboard.css') }}
	{{ HTML::style('/css/selectize.css') }}
	{{ HTML::style('/css/pages/signin.css') }}
	{{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600') }}
</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="#">
					{{ Config::get('page.name') }}
				</a>
			</div>
		</div>
	</div>
	<div class="account-container">
		<div class="content clearfix">
    	@section('message')
	      @if(Session::has('success_message'))
	      <div class="alert alert-success">
	        <button type="button" class="close" data-dismiss="alert">×</button>
	        {{ Session::get('success_message') }}
	      </div>
	      @endif
	      @if(Session::has('error_message'))
	      <div class="alert alert-error">
	        <button type="button" class="close" data-dismiss="alert">×</button>
	        {{ Session::get('error_message') }}
	      </div>
	      @endif

	      @if($errors->count())
	      <div class="alert alert-error">
	        <button type="button" class="close" data-dismiss="alert">×</button>
	        @lang('messages.form.error')
	        {{ HTML::ul($errors->all()) }}
	      </div>
	      @endif
	    @show
			<form action="#" method="post">
				<h1>@lang('auth.title')</h1>
				<div class="login-fields">
					<p>@lang('auth.description')</p>
					<div class="field">
						<label for="username">@lang('auth.username')</label>
						<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
					</div>
					<div class="field">
						<label for="password">@lang('auth.password'):</label>
						<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
					</div>
				</div>
				<div class="login-actions">
					<span class="login-checkbox">
						<input id="remember-me" name="remember-me" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
						<label class="choice" for="remember-me">@lang('auth.remember')</label>
					</span>
					<button class="button btn btn-success btn-large">@lang('form.button.signin')</button>
				</div>
			</form>
		</div>
	</div>
	{{ HTML::script('/js/jquery.js') }}
	{{ HTML::script('/js/bootstrap.js') }}
	{{ HTML::script('/js/signin.js') }}
	</body>
</html>
