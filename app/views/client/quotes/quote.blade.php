<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="format-detection" content="address=no;email=no;telephone=no" />
<title>Invoice #051</title>
{{ HTML::style('/css/email/invoice.css') }}
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<table id="emailBody" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" class="emailBodyCell"><table width="544" border="0" cellpadding="0" cellspacing="0" class="eBox">
        <tr>
          <td class="topCorners"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="top" class="crn_lftp">{{ HTML::image('/img/email/invoice/header_left.png', null, ['width' => 8, 'height' => 8]) }}</td>
                <td class="emptyCell space16">&nbsp;</td>
                <td align="right" valign="top" class="crn_rgtp">{{ HTML::image('/img/email/invoice/header_right.png', null, ['width' => 8, 'height' => 8]) }}</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td class="eHeader"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td class="eHeaderLogo"><a href="#" style="line-height:50px">IMAGEX</a></td>
              </tr>
            </table></td>
        </tr>
        @if($quote->is_paid)
        <tr>
          <td class="paid">@lang('quotes.paid')</td>
        </tr>
        @endif
        <tr>
          <td class="highlight invoiceHead alignLeft"><h4>@lang('quotes.mail.title', ['code' => $quote->code]) <span>{{ date('d/m/Y H:i', strtotime($quote->created_at)) }}</span></h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="width184 pdRg16" width="65%"><table border="0" cellpadding="0" cellspacing="0" class="tag">
                    <tr>
                      <td width="2" height="2"  align="left" valign="top" class="btnLfTp">{{ HTML::image('/img/email/invoice/tag_lftp.png', null, ['width' => 2, 'height' => 2]) }}</td>
                      <td class="emptyCell">&nbsp;</td>
                      <td width="4" height="8" align="right" valign="top" class="btnRgTp">{{ HTML::image('/img/email/invoice/tag_rgtp.png', null, ['width' => 2, 'height' => 2]) }}</td>
                    </tr>
                    <tr>
                      <td class="emptyCell">&nbsp;</td>
                      <td align="left" valign="middle" class="tagName">@lang('clients.client')</td>
                      <td class="emptyCell">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="left" valign="bottom" class="btnLfBt">{{ HTML::image('/img/email/invoice/tag_lfbt.png', null, ['width' => 2, 'height' => 2]) }}</td>
                      <td class="emptyCell">&nbsp;</td>
                      <td align="right" valign="bottom" class="btnRgBt">{{ HTML::image('/img/email/invoice/tag_rgbt.png', null, ['width' => 2, 'height' => 2]) }}></td>
                    </tr>
                    <tr>
                      <td class="tag_space emptyCell">&nbsp;</td>
                      <td class="tag_space emptyCell">&nbsp;</td>
                      <td class="tag_space emptyCell">&nbsp;</td>
                    </tr>
                  </table>
                  <p><span class="ihBlack">{{ $quote->client->name }}</span><br />
                    {{ $quote->client->email }}<br />
                    {{ $quote->client->address }}
                    </p>
                    </td>
                <td class="width312 width312-right" width="35%">
                  <form method="post" id="payment" action="/cgi-bin/tbk_bp_pago.cgi">
                    <input type="hidden" name="TBK_MONTO" value="{{ $quote->iva_price }}.00" />
                    <input type="hidden" name="TBK_TIPO_TRANSACCION" value="TR_NORMAL" />
                    <input type="hidden" name="TBK_ORDEN_COMPRA" value="{{ $quote->id }}" />
                    <input type="hidden" name="TBK_ID_SESION" value="{{ $quote->id }}" />
                    <input type="hidden" name="TBK_URL_EXITO" value="{{ URL::to('quotes/quote', $quote->id) }}" />
                    <input type="hidden" name="TBK_URL_FRACASO" value="{{ URL::to('quotes/quote', $quote->id) }}" />
                    <a href="javascript:document.getElementById('payment').submit()">{{ HTML::image('/img/webpay.png', null, ['class' => 'webpayButton']) }}</a>
                  </form>
                </td> 
              </tr>
            </table>
            </td>
          <!-- end .highlight-->
        </tr>
        <tr>
          <td class="eBody alignLeft"></td>
          <!-- end .eBody-->
        </tr>
        <tr>
          <td class="blank"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="invoiceTable2">
              <tr>
                <th class="width312">@lang('quotes.summary')</th>
                <th class="width84 alignRight">@lang('quotes.quantity')</th>
                <th class="width84 alignRight">@lang('quotes.subtotal')</th>
              </tr>
              @foreach($quote->items as $item)
              <tr>
                <td class="width312">{{ $item->description }}</td>
                <td class="width84 alignRight"><span class="desktopHide">Quantity: </span>{{ $item->quantity }}</td>
                <td class="width84 alignRight"><span class="desktopHide">Subtotal: </span><span class="amount">{{ $item->price }}</span></td>
              </tr>
              @endforeach
              <tr>
                <td colspan="2" class="subTotal alignRight mobileHide">@lang('quotes.subtotal')<br />
                  @lang('quotes.iva')</td>
                <td class="width84 subTotal alignRight"><span class="desktopHide">Subtotal: </span><span class="amount">{{ $quote->price }}</span><br />
                  <span class="desktopHide">@lang('quotes.iva_total'): </span> <span class="amount">{{ $quote->iva_price - $quote->price }}</span></td>
              </tr>
              <tr>
                <td colspan="2" class="width312 eTotal alignRight"><strong>Total</strong></td>
                <td class="width84 eTotal alignRight"><span class="amount">{{ $quote->iva_price }}</span></td>
              </tr>
            </table></td>
          <!-- end invoice content-->
        </tr>
        <!-- end .eBody -->
        <tr>
          <td class="bottomCorners"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="bottom" class="crn_lfbt">{{ HTML::image('/img/email/invoice/mainBtn_lfbt.png', null, ['width' => 6, 'height' => 6]) }}</td>
                <td class="emptyCell">&nbsp;</td>
                <td align="right" valign="bottom" class="crn_rgbt">{{ HTML::image('/img/email/invoice/mainBtn_rgbt.png', null, ['width' => 6, 'height' => 6]) }}</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td class="eFooter">IMAGEX</td>
        </tr>
      </table>
      <!-- end .eBox --></td>
    <!-- end .emailBodyCell -->
  </tr>
</table>
<!-- end #emailBody -->
</body>
</html>