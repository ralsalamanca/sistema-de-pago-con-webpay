<?php
return array(
	'import'   => 'Importar',
	'validate' => 'Validar',
	'message'  => 'Ingresa el código de la cotización en SAP',
	'success'  => 'Se importó correctamente la cotización',
	'error'	   => 'La cotización no existe en SAP o ya ha sido añadida a esta plataforma'
);