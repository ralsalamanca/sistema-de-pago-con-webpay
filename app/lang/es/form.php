<?php

return array(
	'button' => array(
		'save'		=> 'Guardar',
		'cancel'	=> 'Cancelar',
		'edit'	 	=> 'Editar',
		'remove'	=> 'Borrar',
		'show'		=> 'Mostrar',
		'add'		=> 'Agregar',
		'send'		=> 'Enviar',
		'signin'	=> 'Acceder',
		'logout'	=> 'Desconectar',
		'export'	=> 'Generar planilla',
		'refresh'	=> 'Refrescar'
	),
	'from' => 'Desde',
	'to'   => 'Hasta'
);