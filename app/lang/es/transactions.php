<?php

return array(
	'transactions'	=> 'Transacciones',
	'last'			=> 'últimas transacciones',
	'id'			=> 'ID',
	'date'			=> 'Fecha',
	'description'	=> 'Descripción',
	'gateway'		=> 'Procesador',
	'amount'		=> 'Monto',
	'quote'			=> 'Cotización',
	'paid_at' 		=> 'Pagado El',
	'reversed_at'	=> 'Reversado El',
	'created_at'	=> 'Creado El'
);