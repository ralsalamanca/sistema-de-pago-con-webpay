<?php

return array(
	'quotes' 		=> 'Cotización',
	'paid_quotes'	=> 'Cotizaciones pagadas',
	'unpaid_quotes' => 'Cotizaciones impagas',
	'code'			=> 'Código',
	'id'			=> 'ID',
	'name'	 		=> 'Nombre',
	'description'	=> 'Descripción',
	'sent'			=> 'Enviado',
	'paid'			=> 'Pagado',
	'nonpaid'		=> 'No pagado',
	'client'		=> 'Cliente',
	'items'			=> 'Items',
	'unit_price'	=> 'Precio unitario',
	'quantity'		=> 'Cantidad',
	'price'			=> 'Precio',
	'total'			=> 'Total',
	'sent_at'		=> 'Enviado El',
	'sent_to'		=> 'Enviado A',
	'paid_at'		=> 'Pagado El',
	'created_at'	=> 'Creado El',
	'latest_unpaid' => 'Últimos impagos',
	'latest_paid'   => 'Últimos pagados',
	'iva_total'		=> 'Total + IVA',
	'summary'		=> 'Sumario',
	'subtotal'		=> 'Subtotal',
	'iva'			=> 'IVA',
	'filter'		=> 'Filtro',
	'store'			=> [
		'success' => 'Creado sin errores'
	],
	'destroy'		=> [
		'success' => 'Borrado sin errores'
	],
	'mail'			=> [
		'title' => 'Cotización #:code'
	],
	'send' 			=> [
		'success' => 'La cotización ha sido enviada por correo',
		'error'	  => 'Hubieron problemas para enviar esta cotización, lo más probable es que falte el correo de quien recibe la cotización'
	]
);