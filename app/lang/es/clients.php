<?php

return array(
	'clients'		=> 'Clientes',
	'id'			=> 'ID',
	'fullname'		=> 'Nombre',
	'name'			=> 'Nombre',
	'surname'		=> 'Apellido',
	'email'			=> 'Correo electrónico',
	'rut'			=> 'RUT',
	'password'		=> 'Contraseña',
	'created_at'	=> 'Creado El',
	'client'		=> 'Cliente',
	'store'			=> array(
		'success' => 'Creado sin errores'
	),
	'destroy'		=> array(
		'success' => 'Borrado sin errores'
	)
);