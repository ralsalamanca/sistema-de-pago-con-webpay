<?php

return array(
	'quote_items'	=> 'Items de la cotización',
	'id'			=> 'ID',
	'name'			=> 'Nombre',
	'description'	=> 'Descripción',
	'amount'		=> 'Monto',
	'unit_price'	=> 'Precio unitario',
	'quantity'		=> 'Cantidad'
);