<?php

return array(
	'title' 	  => 'Acceso',
	'description' => 'Porfavor, ingresa tus datos',
	'login'       => [
		'error' => 'El usuario y/o contraseña es/son incorrecto(s)'
	],
	'logout'      => [
		'success' => 'Te has deslogueado correctamente'
	],
	'remember'	  => 'Mantenerme conectado',
	'username'	  => 'Usuario',
	'password'	  => 'Contraseña'
);