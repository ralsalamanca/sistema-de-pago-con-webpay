<?php

return array(
	'quotes' 		=> 'Quotes',
	'code'			=> 'Code',
	'id'			=> 'ID',
	'name'	 		=> 'Name',
	'description'	=> 'Description',
	'sent'			=> 'Sent',
	'paid'			=> 'Paid',
	'nonpaid'		=> 'Non paid',
	'client'		=> 'Client',
	'items'			=> 'Items',
	'unit_price'	=> 'Unit price',
	'quantity'		=> 'Quantity',
	'price'			=> 'Price',
	'total'			=> 'Total',
	'sent_at'		=> 'Sent At',
	'paid_at'		=> 'Paid At',
	'created_at'	=> 'Created At',
	'latest_unpaid' => 'Latest unpaid',
	'latest_paid'   => 'Latest paid',
	'iva_total'		=> 'Total + IVA',
	'summary'		=> 'Summary',
	'subtotal'		=> 'Subtotal',
	'iva'			=> 'IVA',
	'filter'		=> 'Filter',
	'store'			=> [
		'success' => 'Created without errors'
	],
	'destroy'		=> [
		'success' => 'Deleted without errors'
	],
	'mail'			=> [
		'title' => 'Quote #:code'
	],
	'send' 			=> [
		'success' => 'The quote has sent to email'
	]
);