<?php

return array(
	'clients'		=> 'Clients',
	'id'			=> 'ID',
	'fullname'		=> 'Name',
	'name'			=> 'Name',
	'surname'		=> 'Surname',
	'email'			=> 'Email',
	'rut'			=> 'RUT',
	'password'		=> 'Password',
	'created_at'	=> 'Created At',
	'client'		=> 'Client',
	'store'			=> array(
		'success' => 'Created without errors'
	),
	'destroy'		=> array(
		'success' => 'Deleted without errors'
	)
);