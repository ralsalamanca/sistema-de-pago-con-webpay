<?php

return array(
	'transactions'	=> 'Transactions',
	'last'			=> 'Last transactions',
	'id'			=> 'ID',
	'date'			=> 'Date',
	'gateway'		=> 'Gateway',
	'amount'		=> 'Amount',
	'quote'			=> 'Quote',
	'paid_at' 		=> 'Paid At',
	'reversed_at'	=> 'Reversed At'
);