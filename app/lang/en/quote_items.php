<?php

return array(
	'quote_items'	=> 'Quote items',
	'id'			=> 'ID',
	'name'			=> 'Name',
	'description'	=> 'Description',
	'amount'		=> 'Amount',
	'unit_price'	=> 'Unit price',
	'quantity'		=> 'Quantity'
);