<?php

return array(
	'button' => array(
		'save'		=> 'Save',
		'cancel'	=> 'Cancel',
		'edit'	 	=> 'Edit',
		'remove'	=> 'Remove',
		'show'		=> 'Show',
		'add'		=> 'Add',
		'send'		=> 'Send',
		'signin'	=> 'Sign in',
		'logout'	=> 'Logout',
		'export'	=> 'Export',
		'refresh'	=> 'Refresh'
	),
	'from' => 'From',
	'to'   => 'To'
);