<?php

return array(
	'title' 	  => 'Login',
	'description' => 'Please provide your details',
	'login'       => [
		'error' => 'The username and/or password is incorrect'
	],
	'logout'      => [
		'success' => 'Logout without errors'
	],
	'remember'	  => 'Keep me signed in',
	'username'	  => 'Username',
	'password'	  => 'Password'
);