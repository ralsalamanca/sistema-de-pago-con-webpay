<?php
use Illuminate\Support\Facades\View;

class QuotesController extends BaseController{
	public function quote($id){
		if($quote = Quote::findOrFail($id)){
			return View::make('client.quotes.quote', compact('quote'));
		}
	}
}