<?php

class AuthController extends BaseController{
	public function index(){
        if (Auth::check())
        {
            return Redirect::to('/admin');
        }
        return View::make('login');
	}
	public function login(){
        if(Auth::attempt([
            'username' => Input::get('username'),
            'password' => Input::get('password')
        ], Input::get('remember-me', 0))){
            return Redirect::to('/admin');
        }
        return Redirect::to('login')
                    ->with('error_message', Lang::get('auth.login.error'))
                    ->withInput();
	}
	public function logout(){
		Auth::logout();
		return Redirect::to('login')
				->with('success_message', Lang::get('auth.logout.success'));
	}
}