<?php
namespace IPN;

use Transaction, Quote;
use Validator, Input, Redirect, Session, Lang, Mail, DateTime;

use Illuminate\Support\Facades\View;

use AdminController;

class WebpayController extends AdminController{
	public function notification(){
		if($quote = Quote::find(Input::get('TBK_ORDEN_COMPRA'))){
			$transaction = new Transaction;
			$transaction->paid_at = new DateTime();
			$transaction->gateway = 'Webpay';
			$transaction->amount  = (int)substr(Input::get('TBK_MONTO'), 0, -2);

			$quote->transactions()->save($transaction);

			if((int)$transaction->amount == (int)$quote->iva_price){
				$quote->paid_at = new DateTime();
				$quote->save();
			}

			return 'ACEPTADO';
		}
		else{
			return 'RECHAZADO';
		}

	}
}