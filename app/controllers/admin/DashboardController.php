<?php
namespace Admin;

use Illuminate\Support\Facades\View;

use Transaction, Quote;
use Input, Redirect, Lang;

use AdminController;
class DashboardController extends AdminController {

	public function index()
	{
		$transactions   = Transaction::take(10)->get();
		$unpaid_quotes  = Quote::take(10)->ofUnpaid()->get();
		$paid_quotes    = Quote::take(10)->ofPaid()->get();

		return View::make('admin.dashboard', compact('transactions', 'unpaid_quotes', 'paid_quotes'));
	}
	public function history(){
		$from = Input::get('from');
		$to   = Input::get('to');

		if(!empty($from) and !empty($to)){
			$transactions   = Transaction::whereBetween('paid_at', array($from, $to))->take(10)->get();
			$unpaid_quotes  = Quote::whereBetween('created_at', array($from, $to))->take(10)->ofUnpaid()->get();
			$paid_quotes    = Quote::whereBetween('created_at', array($from, $to))->take(10)->ofPaid()->get();
			return View::make('admin.dashboard', compact('transactions', 'unpaid_quotes', 'paid_quotes'));
		}

		return Redirect::to('admin')->with('error_message', Lang::get('dashboard.select_dates'));
	}

}
