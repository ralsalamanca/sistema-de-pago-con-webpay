<?php
namespace Admin;

use Quote, Client, QuoteItem;

use Validator, Input, Redirect, Session, Lang;

use Illuminate\Support\Facades\View;

use AdminController;
class QuotesController extends AdminController{
	public function index(){
		$quotes = Quote::all();
		return View::make('admin.quotes.index', compact('quotes'));
	}
	public function create(){
        $clients = Client::all();
		return View::make('admin.quotes.create', compact('clients'));
	}
	public function store(){
        $rules = array(
            'client_id'     => 'required',
            'name'          => 'required'
        );
        foreach(Input::get('quote_item') as $i => $quote_item){
            $rules['quote_item.' . $i . '.name']        = 'required';
            $rules['quote_item.' . $i . '.amount']      = 'required|numeric';
        }
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('admin/quotes/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else {
            $quote = new Quote;
            $quote->name 		= Input::get('name');
            $quote->description	= Input::get('description');
            $quote->client_id	= Input::get('client_id');
            $quote->save();
            foreach(Input::get('quote_item') as $quote_item){
                extract($quote_item);
                $quote_item = new QuoteItem;
                $quote_item->name        = $name;
                $quote_item->description = $description;
                $quote_item->amount      = $amount;
                $quote->items()->save($quote_item);
            }

            Session::flash('success_message', Lang::get('quotes.store.success'));
            return Redirect::to('admin/quotes');
        }
	}
    public function show($id){
        $quote = Quote::findOrFail($id);

        return View::make('admin.quotes.show', compact('quote'));
    }
	public function edit($id){
        $quote   = Quote::findOrFail($id);
        $clients = Client::all();

        return View::make('admin.quotes.edit', compact('quote', 'clients'));
    }
    public function update($id){
        $quote = Quote::findOrFail($id);
        #exit($id);
        $rules = array(
            'client_id'     => 'required',
            'name'          => 'required'
        );
        foreach(Input::get('quote_item') as $i => $item){
            if(!(empty($item['name']) and empty($item['description']) and empty($item['amount']))){
                $rules['quote_item.' . $i . '.name']        = 'required';
                $rules['quote_item.' . $i . '.amount']      = 'required|numeric';
            }
        }
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::route('admin..quotes.update', $id)
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else {
            $quote->name        = Input::get('name');
            $quote->description = Input::get('description');
            $quote->client_id   = Input::get('client_id');
            $quote->save();
            foreach(Input::get('quote_item') as $item){
                if($quote_item = QuoteItem::findOrNew((@$item['id'] ? $item['id'] : false))){
                    if(empty($item['name']) and empty($item['description']) and empty($item['amount'])){
                        $quote_item->delete();
                    }
                    else{
                        $quote_item->name        = $item['name'];
                        $quote_item->description = $item['description'];
                        $quote_item->amount      = $item['amount'];
                        if($quote_item->quote_id)
                            $quote_item->save();
                        else
                          $quote->items()->save($quote_item);
                    }
                }
            }
            Session::flash('success_message', Lang::get('quotes.store.success'));

            return Redirect::route('admin..quotes.update', $id);
        }
    }
    public function destroy($id){
        $quote = Quote::findOrFail($id);
        $quote->delete();

        Session::flash('success_message', Lang::get('quotes.destroy.success'));

        return Redirect::to('admin/quotes');
    }
}
