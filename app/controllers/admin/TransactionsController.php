<?php
namespace Admin;

use Transaction;

use Validator, Input, Redirect, Session, Lang;

use Illuminate\Support\Facades\View;

use AdminController;

class TransactionsController extends AdminController{
	public function index(){
        $transactions       = Transaction::all();
		return View::make('admin.transactions.index', compact('transactions'));
	}
}
