<?php

namespace Admin;

use Quote, Transaction;

use Validator, Input, Redirect, Session, Lang, Excel;

use Illuminate\Support\Facades\View;

use AdminController;

class ExportController extends AdminController{
	public function index(){
		Excel::create(Lang::get('export.name'), function($excel) {
			$excel->sheet(Lang::get('quotes.unpaid_quotes'), function($sheet) {
				$sheet->appendRow([
					Lang::get('quotes.id'),
					Lang::get('quotes.code'),
					Lang::get('quotes.client'),
					Lang::get('quotes.description'),
					Lang::get('quotes.address'),
					Lang::get('quotes.sent_to'),
					Lang::get('quotes.sent_at'),
					Lang::get('quotes.created_at'),
				]);
				foreach(Quote::ofUnpaid()->get() as $quote){
					$sheet->appendRow([
						$quote->id,
						$quote->code,
						$quote->client->name,
						$quote->description,
						$quote->address,
						$quote->sent_to,
						$quote->sent_at,
						$quote->created_at->toDateTimeString()
					]);
				}
			});
			$excel->sheet(Lang::get('quotes.paid_quotes'), function($sheet) {
				$sheet->appendRow([
					Lang::get('quotes.id'),
					Lang::get('quotes.code'),
					Lang::get('quotes.client'),
					Lang::get('quotes.description'),
					Lang::get('quotes.address'),
					Lang::get('quotes.sent_to'),
					Lang::get('quotes.sent_at'),
					Lang::get('quotes.paid_at'),
					Lang::get('quotes.created_at'),
				]);
				foreach(Quote::ofPaid()->get() as $quote){
					$sheet->appendRow([
						$quote->id,
						$quote->code,
						$quote->client->name,
						$quote->description,
						$quote->address,
						$quote->sent_to,
						$quote->sent_at,
						$quote->paid_at,
						$quote->created_at->toDateTimeString()
					]);
				}
			});
			$excel->sheet(Lang::get('transactions.transactions'), function($sheet) {
				$sheet->appendRow([
					Lang::get('transactions.id'),
					Lang::get('transactions.description'),
					Lang::get('transactions.amount'),
					Lang::get('transactions.gateway'),
					Lang::get('transactions.paid_at'),
					Lang::get('transactions.created_at'),
				]);
				foreach(Transaction::all() as $transaction){
					$sheet->appendRow([
						$transaction->id,
						$transaction->quote->description,
						$transaction->amount,
						$transaction->gateway,
						$transaction->paid_at,
						$transaction->created_at->toDateTimeString()
					]);
				}
			});
		})->export('xls');
	}
}