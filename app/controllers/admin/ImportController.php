<?php
namespace Admin;

use SAP, Quote, QuoteItem, Client;

use Validator, Input, Redirect, Session, Lang;

use Illuminate\Support\Facades\View;

use AdminController;

class ImportController extends AdminController{
    public function quote(){
    	$quote_id = Input::get('quote_id');

    	if($remoteQuote = SAP::getQuote($quote_id)){
    		if(!Quote::findByCode($remoteQuote->DocNum)){
	    		if(!($client = Client::findByRut(str_replace(['.', '-'], '', $remoteQuote->CardCode))))
	    			$client = new Client;
	    		$client->name     = $remoteQuote->CardName;
	    		$client->rut      = $remoteQuote->CardCode;
	    		$client->password = 123456;
	    		$client->email    = $remoteQuote->E_Mail ? $remoteQuote->E_Mail : '';

	    		if($client->save()){
		    		$quote = new Quote;
		    		$quote->code      = $quote->description = $remoteQuote->DocNum;
		    		$quote->address   = $remoteQuote->Address;
		    		$quote->sent_to   = $remoteQuote->E_Mail ? $remoteQuote->E_Mail : '';

		    		if($client->quotes()->save($quote)){
				    	if($items = SAP::getQuoteItems($quote_id)){
				    		foreach($items as $remoteItem){
				    			$item = new QuoteItem;
				    			$item->code        = $remoteItem->ItemCode;
				    			$item->description = $remoteItem->Dscription;
				    			$item->quantity    = $remoteItem->Quantity;
				    			$item->unit_price  = $remoteItem->Price;
				    			$item->price       = $remoteItem->LineTotal;
				    			$quote->items()->save($item);
				    		}
				    		return Redirect::route('admin..quotes.mail', $quote->id)->with('success_message', Lang::get('import.success'));
				    	}
			    	}
		    	}
		    }
    	}
    	return Redirect::to('admin')->with('error_message', Lang::get('import.error'));
    }
}
