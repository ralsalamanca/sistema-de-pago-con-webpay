<?php
namespace Admin;

use Client;

use Validator, Input, Redirect, Session, Lang;

use Illuminate\Support\Facades\View;

use AdminController;
class ClientsController extends AdminController{
	public function index(){
		$clients = Client::all();
		return View::make('admin.clients.index', compact('clients'));
	}
	public function create(){
		return View::make('admin.clients.create');
	}
	public function store(){
        $validator = Validator::make(Input::all(), array(
            'name'			=> 'required',
            'surname'		=> 'required',
            'rut'			=> 'required|unique:clients',
            'email'			=> 'required|email|unique:clients',
            'password'		=> 'required'
        ));

        if ($validator->fails()) {
            return Redirect::to('admin/clients/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else {
            $client = new Client;
            $client->name 		= Input::get('name');
            $client->surname	= Input::get('surname');
            $client->rut		= Input::get('rut');
            $client->email		= Input::get('email');
            $client->password	= Input::get('password');
            $client->save();

            Session::flash('success_message', Lang::get('clients.store.success'));

            return Redirect::to('admin/clients');
        }
	}
        	public function show($id){
        $client = Client::findOrFail($id);

        return View::make('admin.clients.show', compact('client'));
    }
	public function edit($id){
        $client = Client::findOrFail($id);

        return View::make('admin.clients.edit', compact('client'));
    }
    public function update($id){
        $client = Client::findOrFail($id);

        $validator = Validator::make(Input::all(), array(
            'name'          => 'required',
            'surname'       => 'required',
            'rut'           => "required|unique:clients,rut,{$id}",
            'email'         => "required|email|unique:clients,email,{$id}",
            'password'      => 'required'
        ));

        if ($validator->fails()) {
            return Redirect::to("admin/clients/{$id}/edit")
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else {
            $client->name       = Input::get('name');
            $client->surname    = Input::get('surname');
            $client->rut        = Input::get('rut');
            $client->email      = Input::get('email');
            $client->password   = Input::get('password');
            $client->save();

            Session::flash('success_message', Lang::get('clients.store.success'));

            return Redirect::to("admin/clients/{$id}/edit");
        }
    }
    public function destroy($id){
        $client = Client::findOrFail($id);
        $client->delete();

        Session::flash('success_message', Lang::get('clients.destroy.success'));

        return Redirect::to('admin/clients');
    }
}
