<?php
namespace Admin;

use Quote;

use Validator, Input, Redirect, Session, Lang, Mail;

use Illuminate\Support\Facades\View;

use AdminController;
class MailQuoteController extends AdminController{
    public function preview($id){
        if($quote = Quote::findOrFail($id)){
            return View::make('admin..quotes.mail', compact('quote'));
        }
    }

    public function send($id){
        if($quote = Quote::findOrFail($id)){
            if(Input::get('email')){
            	Mail::send('emails.quotes.quote', compact('quote'), function($message) use ($quote){
                    $message->to(Input::get('email'), $quote->client->name)
                            ->subject(Lang::get('quotes.mail.title', ['code' => $quote->code]));
                });

                $quote->sent_at = new DateTime();
                $quote->save();

                return Redirect::route('admin..quotes.mail', $id)->with('success_message', Lang::get('quotes.send.success'));
            }
            else{
                return Redirect::route('admin..quotes.mail', $id)->with('error_message', Lang::get('quotes.send.error'));
            }
        }
    }
}