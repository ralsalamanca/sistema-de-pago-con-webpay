<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class QuoteItem extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'quote_items';

	public $timestamps = false;

	public function quote(){
		return $this->belongsTo('Quote');
	}
}
