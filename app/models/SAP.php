<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class SAP{
	public static function getQuoteItems($quote_id){
		return DB::connection('sap')->table('OQUT as T0')->select(
			'T1.ItemCode',
			'T1.Dscription',
			'T1.Quantity',
			'T1.Price',
			'T1.LineTotal'
		)
		->join('QUT1 as T1', 'T0.DocEntry', '=', 'T1.DocEntry')
		->join('OCRD as T2', 'T0.CardCode', '=', 'T2.CardCode')
		->where('T0.DocNum', $quote_id)
		->get();
	}

	public static function getQuote($quote_id){
		return DB::connection('sap')->table('OQUT as T0')->select(
			'T0.DocNum',
			'T0.DocDate',
			'T0.CardCode',
			'T0.CardName',
			'T0.Address',
			'T2.E_Mail',
			'T0.DocTotal'
		)
		->join('OCRD as T2', 'T0.CardCode', '=', 'T2.CardCode')
		->WHERE('T0.DocNum', $quote_id)
		->first();
	}
}