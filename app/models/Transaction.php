<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class Transaction extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transactions';

	public $timestamps = false;

	public function quote(){
		return $this->belongsTo('Quote');
	}
}
