<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class Client extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clients';

	public function quotes(){
		return $this->hasMany('Quote');
	}

	public function transactions(){
		return $this->hasManyThrough('Transaction', 'Quote');
	}

	public function getFullnameAttribute(){
		return $this->name . ' ' . $this->surname;
	}
	public function setPasswordAttribute($password) {
		$this->attributes['password'] = Hash::make($password);
	}
	public static function findByRut($rut){
		return self::where('rut', $rut)->first();
	}
}
