<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class Quote extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'quotes';

	// Custom attributes
	public function getPriceAttribute(){
		$price = 0;
		foreach($this->items as $item){
			$price += $item->price;
		}
		return $price;
	}

	public function getIvaPriceAttribute(){
		return round($this->price * 1.19);
	}

	public function getIsPaidAttribute(){
		return '0000-00-00 00:00:00' == $this->paid_at ? false : true;
	}

	// Model attributes
	public function items(){
		return $this->hasMany('QuoteItem');
	}

	public function transactions(){
		return $this->hasMany('Transaction');
	}

	public function client(){
		return $this->belongsTo('Client');
	}
	// General
	public static function findByCode($code){
		return self::where('code', $code)->first();
	}

	// Scopes
	public function scopeOfUnpaid($query){
		return $query->where('paid_at', '=', '0000-00-00 00:00:00');
	}

	public function scopeOfPaid($query){
		return $query->where('paid_at', '!=', '0000-00-00 00:00:00');
	}
}
