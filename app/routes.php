<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function(){
    if(Auth::check())
        return Redirect::to('/admin');
    else
        return Redirect::to('/login');
});

// Client
Route::any('/quotes/quote/{id}', 'QuotesController@quote');

// Admin login
Route::get('login', 'AuthController@index');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

// Admin
Route::group(array('before' => 'auth'), function(){
    Route::group(array('prefix' => 'admin'), function(){
        Route::get('/', 'Admin\DashboardController@index');
        Route::post('/', 'Admin\DashboardController@history');
        Route::resource('/clients', 'Admin\ClientsController');
        Route::get('/quotes/mail/{id}', [
            'uses' => 'Admin\MailQuoteController@preview',
            'as'   => 'admin..quotes.mail'
        ]);
        Route::post('/quotes/mail/{id}', [
            'uses' => 'Admin\MailQuoteController@send',
            'as'   => 'admin..quotes.mail'
        ]);    Route::resource('/quotes', 'Admin\QuotesController');
        Route::get('/transactions', 'Admin\TransactionsController@index');
        Route::post('/import/quote', [
        	'uses' => 'Admin\ImportController@quote',
            'as' => 'admin..import.quote'
        ]);
        Route::get('/export', 'Admin\ExportController@index');
    });
});


// IPNs
Route::group(array('prefix' => 'ipn'), function(){
    Route::any('/webpay', 'IPN\WebpayController@notification');
});