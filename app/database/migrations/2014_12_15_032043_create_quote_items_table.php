<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quote_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quote_id')->unsigned();
			$table->foreign('quote_id')->references('id')->on('quotes');
			$table->string('code');
			$table->string('description');
			$table->integer('quantity');
			$table->float('unit_price', 24, 2);
			$table->float('price', 24, 2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quote_items');
	}

}
